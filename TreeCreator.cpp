#include "settings.h"

void TreeCreator(TString FileName, unsigned int SAMPLES_PER_EVENT=1024){
  /*
   *  Crea un file AAMMGG_i.root con un TTree chiamato lab_tree
   *  - FileName deve essere del tipo AAMMGG_i.txt 
   *             dove i è l'i-esimo file acquisito nella giornata; 
   *             il file deve essere costituito da una sola colonna  
   *  - SAMPLES_PER_EVENT è il numero di campionamenti per ogni evento.
   *             di default il valore è 1024.
   *
   *  Esempio di compilazione di questa macro:
   *    root [ ] .L TreeCreator.cpp
   *    root [ ] TreeCreator("AAMMGG_i.txt")
   *
   *  Il file AAMMGG_i.root può essere letto con
   *    $        root AAMMGG_i.root 
   *  una volta letto, con 
   *    root [ ] .ls
   *  si vedono tutti gli oggetti. Si possono visualizzare agevolmente con un Tgraph: 
   *    root [ ] lab_tree->Draw("Voltaggio:Tempo","Entry$==1", "l")
   *    root [ ] lab_tree->Draw("Carica")   
   *
   */

  TString   RootFileName( FileName(0,8)+".root" );
  TFile    *RootFile = new TFile( RootFileName,"recreate" );
  ifstream  InputFile;
  InputFile.open(FileName);

  TTree *tree = new TTree("lab_tree",
			  "Acquisizione dati "+FileName(0,8)
			  );
  
  unsigned int Vbit[SAMPLES_PER_EVENT];         //bit
  double V[SAMPLES_PER_EVENT];                  //V
  double V_pos[SAMPLES_PER_EVENT];              //V
  double V_since_max[500] = {0.};               //V
  double t[SAMPLES_PER_EVENT];                  //s
  double I[SAMPLES_PER_EVENT];                  //A  
  double c = 0.;                                //C
  double RumoreMedio = 0.;                      //V
  double AmpiezzaMax = 0.;                      //V
  double AmpiezzaPosMax = 0.;                   //V
  unsigned int max = 0; //indice corrispondente il massimo
  
  tree->Branch("Vbit",             Vbit,               "Vbit[1024]/i");
  tree->Branch("Voltaggio",        V,             "Voltaggio[1024]/D");
  tree->Branch("VoltaggioPos",     V_pos,      "VoltaggioPos[1024]/D");
  tree->Branch("VoltaggioSinceMax",V_since_max, "VoltaggioPos[500]/D");
  tree->Branch("Tempo",            t,                 "Tempo[1024]/D");
  tree->Branch("CorrentePos",      I,           "CorrentePos[1024]/D");
  tree->Branch("Carica",           &c,                     "Carica/D");
  tree->Branch("RumoreMedio",      &RumoreMedio,      "RumoreMedio/D");
  tree->Branch("AmpiezzaMax",      &AmpiezzaMax,      "AmpiezzaMax/D");
  tree->Branch("AmpiezzaPosMax",   &AmpiezzaPosMax,"AmpiezzaPosMax/D");
  tree->Branch("max_index",        &max,                "max_index/i");
      
  double Resistenza = 50.;                      //Ohm
  unsigned int NumberOfEvents = 0;
  
  while(InputFile.good()){

    //calcolo il rumore medio sui primi 100 campionamenti
    double Rumore = 0.;
    for(int i=0; i<100; i++){

      InputFile >> Vbit[i];
      V[i] = 2.*double(Vbit[i])/4096. - 1.;

      Rumore += V[i];
      
    }
    
    RumoreMedio = Rumore/100.;
    AmpiezzaMax = 0.;
    AmpiezzaPosMax = 0.;
    c = 0.;
    max = 0;
    
    for(int j=0; j<SAMPLES_PER_EVENT; j++){
      
      //non faccio due volte lo stesso lavoro
      if(j>=100){
	InputFile >> Vbit[j];
	V[j] = 2.*double(Vbit[j])/4096. - 1.;
      }

      t[j]     = SECONDS_BETWEEN_SAMPLES*j;
      V_pos[j] = -(V[j]-RumoreMedio);
      I[j]     = V_pos[j]/Resistenza;
      c       += I[j]*SECONDS_BETWEEN_SAMPLES;

      
      if (V[j] < -AmpiezzaMax){
	AmpiezzaMax = -V[j];
      }

      if (V_pos[j] >= AmpiezzaPosMax){
	AmpiezzaPosMax = V_pos[j];
	max = j;
	//printf("max: %d",max)
      }

    }

    for (int k=0; k<500; k++ )  V_since_max[k] = V_pos[max+k];
    
    tree->Fill();
    
  }
  
  InputFile.close();
  RootFile->Write();
  
  //puliamo la memoria
  delete tree;
  delete RootFile;

  return;
}


/*
   Divertimenti:
   root [3] treetree->Draw("Voltaggio:Iteration$","Entry$==1","l")
   root [4] treetree->Draw("Voltaggio:Iteration$","","l")
   root [5] treetree->Draw("Voltaggio:Iteration$*4e-9","","l")
   root [6] treetree->Draw("Sum$(Voltaggio)","","")
   root [8] treetree->Draw("-1*Sum$(Voltaggio-2230)","","")
   root [9] treetree->Draw("Voltaggio","Iteration$<50","")
   root [10] TH1F* h=new TH1F("h","h",3000,0,3000)
   root [11] treetree->Project("h","Voltaggio","Iteration$<50")
   root [12] h->GetMean()
   (double) 2230.6111
   root [13] h->GetRMS()
   (double) 1.4317599
   root [14] h->Draw()
*/

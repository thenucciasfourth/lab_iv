//macro Root per leggere file

void LeggiFile(TString fname){
  //TString è una classe molto utlie di root simile a str di python
  ifstream  file;  //input file stream, oggetto c++ comodo per leggere file
  TH1F* h1 = new TH1F("h1", "Colonna1", 400, -10,10); 
  TH1F* h2 = new TH1F("h2", "Colonna2", 400, -10,10);
  TH2F* hh = new TH2F("hh", "Colonna1vsColonna2", 400, -10,10,400,-10,10);

  
  file.open(fname);
  
  while(file.good()) { //ifstream::good verifica se il file è leggibile o no. ritorna vero o falso
    double a,b;
    file >> a >> b; //il contenuto di file lo mando dentro ad a, e cerca automaticamente di convertire in double. Va a capo automaticamente

    h1->Fill(a);
    h2->Fill(b);
    hh->Fill(a,b);
  }

  file.close();

  TCanvas* c1 = new TCanvas("c1");
  TCanvas* c2 = new TCanvas("c2");
  TCanvas* c3 = new TCanvas("c3");
  
  c1->cd(); //esegui il comando cd() su canvas. Vai su un altro canvas
  h1->Draw();

  c2->cd();
  h2->Draw();

  c3->cd();
  hh->Draw("colz");
  
  return;
}

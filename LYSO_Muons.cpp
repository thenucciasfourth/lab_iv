#include "settings.h"

double landau_carica(double *x, double *par){
  /*landau per fittare la carica in uscita dal fotomoltiplicatore*/
  double fitval = par[0]*TMath::Landau(x[0],par[1],par[2]);
  return fitval;
}

void LYSO_Muons(TString RootFileName="220321/220321_1.root"){
  /* Analisi dati relativa all'acquisizione del 2022-03-14(preliminare) e del
   * 2022-03-21(definitiva) sul rilevamento dei muoni con il LYSO.
   *                                                              
   * Si articola in tre step:                                               
   * [1] calcolo dell'evento medio per individuare l'evento di trigger medi o e calcolo
   *     di tau_preliminare utile nello step 2                              
   * [2] istogramma della carica rilevata dall'uscita del fotomoltiplicatore
   * [3] calcolo della tau caratteristica del LYSO                          
   *                                                                        
   * opera su un file di tipo .root contenente un TTree di nome "lab_tree"  
   * generato da TreeCreator() in TreeCreator.cpp                           
   *                                                                        
   * le variabili in maiuscolo sono importate da settings.h                 
   *                                                                        
   */  
  TFile* RootFile = new TFile(RootFileName, "read");
  TTree* lab_tree = (TTree*)RootFile -> Get("lab_tree");
  
  double t[SAMPLES_PER_EVENT];     //s
  double I[SAMPLES_PER_EVENT];     //A
  double V_pos[SAMPLES_PER_EVENT]; //V
  double V_since_max[500]={0.};    //V
  double AmpiezzaMax = 0.;         //V
  unsigned int max = 0;
    
  lab_tree->SetBranchAddress("Tempo",             t            );
  lab_tree->SetBranchAddress("CorrentePos",       I            );
  lab_tree->SetBranchAddress("VoltaggioPos",      V_pos        );
  lab_tree->SetBranchAddress("VoltaggioSinceMax", &V_since_max );
  lab_tree->SetBranchAddress("AmpiezzaMax",       &AmpiezzaMax );
  lab_tree->SetBranchAddress("max_index",         &max         );

  unsigned int NumberOfEvents = lab_tree->GetEntries(); //variabile di lavoro

  /// EVENTO MEDIO
  
  TCanvas* evento_medio_canvas = new TCanvas("evento_medio_canvas", "Evento Medio");
  
  double VMedio[500] = {0.};       //V
  double t_500[500];
  for (int l =0; l<500; l++) t_500[l]=l*SECONDS_BETWEEN_SAMPLES;
  
  for(int i=0; i<NumberOfEvents; i++){
    
    lab_tree->GetEntry(i);
    
    for(int j=0; j<500; j++){
      VMedio[j] += V_since_max[j]/double(NumberOfEvents);
    }
    
  }
  
  TGraph* evento_medio = new TGraph(500, t_500, VMedio);

  evento_medio->Fit("expo","","", 0, 0.3e-6);

  evento_medio->SetTitle("Muoni LYSO: evento medio; tempo [s]; Voltaggio [V]");   
  gStyle->SetOptStat(11);                                                                   
  gStyle->SetOptFit(111);
  evento_medio->SetMarkerStyle(5);                                                          
  evento_medio->SetMarkerSize(1);
  evento_medio->Draw("ALP");
  
  TF1* expo_medio = (TF1*)evento_medio->GetListOfFunctions()->FindObject("expo");           
  double preliminar_tau = -1./expo_medio->GetParameter(1);                                  
  printf("tau preliminare = %f*10^-8 s\n",preliminar_tau*pow(10,8));

  ///  CARICA RILEVATA
  
  TCanvas *carica_canvas = new TCanvas("carica_canvas","Istogramma carica rilevata");
  TH1 *calibrated_charge = new TH1D("calibrated_charge",
				    "Muoni LYSO: carica rilevata",
				    ceil(TMath::Sqrt(NumberOfEvents)),
				    0, 1.6e-9);
  carica_canvas->cd();
  
  int start = int(max -   preliminar_tau/SECONDS_BETWEEN_SAMPLES);
  int stop  = int(max + 5*preliminar_tau/SECONDS_BETWEEN_SAMPLES);
  printf("start: %d \n",start);
  printf("stop: %d \n",stop);
  
  double charge = 0.;

  for(int i=0; i<NumberOfEvents; i++){

    lab_tree->GetEntry(i);

    //TAGLIO EVENTI CHE SATURANO
    //if (AmpiezzaMax<-0.98) continue; 
    
    for(int j=start; j<stop; j++){
      charge += I[j]*SECONDS_BETWEEN_SAMPLES;
    }
    calibrated_charge->Fill(charge);
    charge = 0.;
  }

  TF1* func = new TF1("fit_function",
		      landau_carica,
		      0.6e-9, 1.5e-9, //intervallo
                      3 );           //numero di parametri  
  func->SetParNames("Constant","MPV","Sigma");
  func->SetParameters(13000,7.68e-10,5.9e-11);
  
  calibrated_charge->Fit(func,"","",0.7e-9,1.3e-9);
  gStyle->SetOptFit(111);
  gStyle->SetOptStat(11);
  calibrated_charge->GetXaxis()->SetTitle("Carica rilevata [C]");
  calibrated_charge->GetYaxis()->SetTitle("Entries");
  calibrated_charge->Draw();  

  double carica_mpv   = func->GetParameter(1);
  double carica_sigma = func->GetParameter(2);
  
  //////////CALCOLO TAU
  
  TCanvas* istogramma_tau_canvas = new TCanvas("istogramma_tau_canvas",
					       "Istogramma tau");
  istogramma_tau_canvas->cd();
  
  TH1D *tau_histo = new TH1D("tau_histo","Muoni LYSO: istogramma #tau",
			     ceil(TMath::Sqrt(NumberOfEvents)),
			     2e-8, 9e-8);
  //variabili di lavoro
  TGraph* an_event = NULL;
  TF1* expo = NULL; 
  double a_tau = 0.;

  
  for(int i=0; i<NumberOfEvents; i++){
    
    printf("\nEVENTO n %d \n",i);

    lab_tree->GetEntry(i);  

    an_event = new TGraph(500, t_500, V_since_max);
    an_event->Fit("expo","","",0,5*preliminar_tau);

    expo = (TF1*)an_event->GetListOfFunctions()->FindObject("expo");
    a_tau = -1./expo->GetParameter(1);
    tau_histo->Fill(a_tau);
    printf("tau: %f*10^-6 \n", a_tau*pow(10,6));
      
    delete an_event;

  }
  
  tau_histo->GetXaxis()->SetTitle("#tau [s]");
  tau_histo->GetYaxis()->SetTitle("Entries");
  tau_histo->Fit("gaus","","", 43e-9,54e-9);
  gStyle->SetOptStat(11);
  gStyle->SetOptFit(111);
  tau_histo->Draw();
 
  TF1* gaus_tau = (TF1*)tau_histo->GetListOfFunctions()->FindObject("gaus");
  double tau_media = gaus_tau->GetParameter(1);
  double tau_sigma = gaus_tau->GetParameter(2);
    

  printf("---------RIEPILOGO---------\n");
  printf("tau preliminare = %f*10e-8 s\n",
	 preliminar_tau*pow(10,8));
  
  printf("tau media: %f*10e-8 +/- %f*10e-9 [s]\n",
	 tau_media*pow(10,8),
	 tau_sigma*pow(10,9));   
    
  printf("carica MPV: %f*10e-10 +/- %f*10e-11 [s]\n",
	 carica_mpv*pow(10,10),
	 carica_sigma*pow(10,11));   
  
  
  //delete calibrated_charge;
  //delete c2;
  //delete gr;
  //delete tau_histo;
  //delete IstogrammaTau;
  //delete lab_tree;
  //delete RootFile;

  return;
}

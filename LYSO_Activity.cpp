#include "settings.h"

void LYSO_Activity(TString RootFileName="211213/211213_1.root"){
  /* Analisi dati relativa all'acquisizione del 2021-12-13 sulla radioattivita' del LYSO.
   * Si articola in tre step:
   * [1] calcolo dell'evento medio per individuare l'evento di trigger medio e calcolo
   *     di tau_preliminare utile nello step 2
   * [2] istogramma della carica rilevata dall'uscita del fotomoltiplicatore
   * [3] calcolo della tau caratteristica del LYSO
   *
   * opera su un file di tipo .root contenente un TTree di nome "lab_tree"
   * generato da TreeCreator() in TreeCreator.cpp
   *
   * le variabili in maiuscolo sono importate da settings.h
   *
   */
  
  TFile* RootFile = new TFile(RootFileName, "read");
  TTree* lab_tree = (TTree*)RootFile -> Get("lab_tree");

  double I[SAMPLES_PER_EVENT];     //C
  double V_pos[SAMPLES_PER_EVENT]; //V
  double t[SAMPLES_PER_EVENT];     //s
  
  lab_tree->SetBranchAddress("CorrentePos",  I      );
  lab_tree->SetBranchAddress("VoltaggioPos", V_pos  );
  lab_tree->SetBranchAddress("Tempo",        t      );

  unsigned int NumberOfEvents = lab_tree->GetEntries();
  
  
  //// EVENTO MEDIO
  /* plotto l'evento medio ed estraggo una tau preliminare che mi servirà a identificare
   * l'evento di trigger nel calcolo della carica emessa dal fotomoltiplicatore
   */

  // error: variable-sized object may not be initialized

  TCanvas* canvas_evento_medio = new TCanvas("canvas_evento_medio", "Evento Medio");
  
  double VMedio[1024] = {0};       //V 
  
  for(int i=0; i<NumberOfEvents; i++){
    
    lab_tree->GetEntry(i); //in questo momento V contiene i 1024 voltaggi dell'evento i-esimo
    
    for(int j=0; j<SAMPLES_PER_EVENT; j++){
      VMedio[j] +=  V_pos[j]/double(NumberOfEvents);
    }
    
  }
  
  TGraph* evento_medio = new TGraph(SAMPLES_PER_EVENT, t, VMedio);

  //fitto con un'expo nella discesa 
  evento_medio->Fit("expo","","", 0.704e-6, 1e-6);
  evento_medio->SetTitle("Radioattivita' LYSO: evento medio; tempo [s]; Voltaggio [V]");
  gStyle->SetOptStat(11);
  gStyle->SetOptFit(111);
  evento_medio->SetMarkerStyle(5);
  evento_medio->SetMarkerSize(1);
  evento_medio->Draw("ALP");
  
  TF1* expo_medio = (TF1*)evento_medio->GetListOfFunctions()->FindObject("expo");
  double preliminar_tau = -1./expo_medio->GetParameter(1);
  printf("tau preliminare = %f*10^-8 s\n",preliminar_tau*pow(10,8));

  /// CARICA EMESSA DAL FOTOMOLTIPLICATORE

  TCanvas *carica_rilevata_canvas = new TCanvas("carica_rilevata_canvas",
						"Istogramma carica rilevata");
  carica_rilevata_canvas->cd();
  
  double pre_trigger_percent = 18./100.;
  
  int start = int(pre_trigger_percent*SAMPLES_PER_EVENT -
		  preliminar_tau/SECONDS_BETWEEN_SAMPLES);
  int stop  = int(pre_trigger_percent*SAMPLES_PER_EVENT +
		  5*preliminar_tau/SECONDS_BETWEEN_SAMPLES);
  printf("start: %d \n",start);
  printf("stop:  %d \n",stop );
  
  TH1* calibrated_charge = new TH1D("calibrated_charge",
				    "Radioattivita' LYSO: carica rilevata",
				    ceil(TMath::Sqrt(NumberOfEvents)),
				    0, 1.6e-9);

  double charge = 0.;

  for(int i=0; i<NumberOfEvents; i++){

    lab_tree->GetEntry(i);
    
    for(int j=start; j<stop; j++){
      charge += I[j]*SECONDS_BETWEEN_SAMPLES;
    }
    calibrated_charge->Fill(charge);
    charge = 0.;
  }

  calibrated_charge->Fit("gaus", "", "", 0.5e-9,0.62e-9);
  gStyle->SetOptFit(111);
  gStyle->SetOptStat(11);
  calibrated_charge->GetXaxis()->SetTitle("Carica rilevata [C]");
  calibrated_charge->GetYaxis()->SetTitle("Entries"); 
  calibrated_charge->Draw();

  TF1* gaus_carica = (TF1*)calibrated_charge->GetListOfFunctions()->FindObject("gaus");
  double carica_media = gaus_carica->GetParameter(1);
  double carica_sigma = gaus_carica->GetParameter(2);
  printf("picco carica: %f*10e-10  +/- %f*10e-11 C\n",
	 carica_media*pow(10,10),
	 carica_sigma*pow(10,11));
  
  /// CALCOLO TAU
  TCanvas *c3 = new TCanvas("c3","istogramma tau");
  c3->cd();

  double a_tau = 0.;
  TH1D *tau_histo = new TH1D("tau_histo","Radioattivita' LYSO: istogramma #tau",
			     ceil(TMath::Sqrt(NumberOfEvents)),
			     2e-8, 9e-8);  

  TGraph* an_event = NULL;
  TF1* expo = NULL;

  for(int i=0; i<NumberOfEvents; i++){
    
    printf("\nEVENTO n %d \n",i);

    lab_tree->GetEntry(i);  
    an_event = new TGraph(SAMPLES_PER_EVENT, t, V_pos);
    an_event->Fit("expo","","", 0.704e-6, 1e-6);

    expo = (TF1*)an_event->GetListOfFunctions()->FindObject("expo");
    a_tau = -1./expo->GetParameter(1);
    tau_histo->Fill(a_tau);
    printf("tau: %f*10^-8 \n", a_tau*pow(10,8));
      
    delete an_event;
    //delete expo; //causa segmentation fault

  }
  
  tau_histo->GetXaxis()->SetTitle("#tau [s]");
  tau_histo->GetYaxis()->SetTitle("Entries"); 

  tau_histo->Fit("gaus", "", "", 3e-8,7e-8);
  gStyle->SetOptStat(11);
  gStyle->SetOptFit(111);
  
  tau_histo->Draw();

  TF1* gaus_tau = (TF1*)tau_histo->GetListOfFunctions()->FindObject("gaus");
  double tau_media = gaus_tau->GetParameter(1);
  double tau_sigma = gaus_tau->GetParameter(2);

  printf("---------RIEPILOGO---------\n");  
  printf("tau preliminare = %f*10e-8 s\n",
	 preliminar_tau*pow(10,8));
  printf("picco carica: %f*10e-10 +/- %f*10e-11 [C]\n",
	 carica_media*pow(10,10),
	 carica_sigma*pow(10,11));
  printf("tau media: %f*10e-8 +/- %f*10e-9 [s]\n",
	 tau_media*pow(10,8),
	 tau_sigma*pow(10,9));

  return;
}
